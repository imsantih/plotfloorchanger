package me.imsanti.dev;

import com.plotsquared.core.location.Location;
import com.plotsquared.core.plot.Plot;
import me.imsanti.dev.utils.PlotUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Vector;

public class Underground implements CommandExecutor {
    PlotFloorChanger plugin;

    FileConfiguration config;

    public Underground(PlotFloorChanger plugin, FileConfiguration config) {
        this.plugin = plugin;
        this.config = config;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Only for players!");
                return true;
            }
            Player player = (Player)sender;
            if (!player.getInventory().getItemInMainHand().getType().isBlock()) {
                player.sendMessage("You have to hold a block in hand!");
                return true;
            }
            ItemStack item = player.getInventory().getItemInMainHand();
            if (PlotFloorChanger.isBlockInBlackList(item.getType())) {
                player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.blacklist")));
                return true;
            }
            Plot plot = new Location(player.getWorld().getName(), (int) player.getLocation().getX(), (int) player.getLocation().getY(), (int) player.getLocation().getZ()).getPlot();
            if (plot == null) {
                player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notonplot")));
                return true;
            }
            if (!plot.isOwner(player.getUniqueId())) {
                player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notyourplot")));
                return true;
            }
            String[] height = this.config.getString("underground").split("-");
            Location corner1 = plot.getCorners()[0];
            Location corner2 = plot.getCorners()[1];
            corner1.setY(Integer.parseInt(height[0]));
            corner2.setY(Integer.parseInt(height[1]));
            PlotFloorChanger.fillBlocks(PlotFloorChanger.convertToBukkitLocation(corner1), PlotFloorChanger.convertToBukkitLocation(corner2), item
                    .getType());
            player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.undergroud_successful")));
            return true;
        }else {
            final Player player = (Player) sender;
            if(args.length == 1) {

                if(!Arrays.toString(Material.values()).contains(args[0].toUpperCase())) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.invalid-material")));
                    return true;
                }


                final Material material = Material.getMaterial(args[0].toUpperCase());
                if (PlotFloorChanger.isBlockInBlackList(material)) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.blacklist")));
                    return true;
                }
                Plot plot = new Location(player.getWorld().getName(), (int) player.getLocation().getX(), (int) player.getLocation().getY(), (int) player.getLocation().getZ()).getPlot();
                if (plot == null) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notonplot")));
                    return true;
                }
                if (!plot.isOwner(player.getUniqueId())) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notyourplot")));
                    return true;
                }
                String[] height = this.config.getString("underground").split("-");
                Location corner1 = plot.getCorners()[0];
                Location corner2 = plot.getCorners()[1];

                corner1.setY(Integer.parseInt(height[0]));
                corner2.setY(Integer.parseInt(height[1]));
                PlotFloorChanger.fillBlocks(PlotFloorChanger.convertToBukkitLocation(corner1), PlotFloorChanger.convertToBukkitLocation(corner2), material
                );
                player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.undergroud_successful")));
                return true;


            }
         }
        return false;
    }
}