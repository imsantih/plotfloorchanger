package me.imsanti.dev;


import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.world.block.BlockTypes;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PlotFloorChanger extends JavaPlugin {
    public static FileConfiguration config;

    public void onEnable() {
        saveDefaultConfig();
        for(final Material mat : Material.values()) {
            Bukkit.broadcastMessage(mat.name());
        }

        config = getConfig();
        getCommand("changeplotfloor").setExecutor(new Floor(this, config));
        getCommand("changeplotunderground").setExecutor(new Underground(this, config));
    }

    public static void fillBlocks(Location loc1, Location loc2, Material mat) {
        EditSession editSession = WorldEdit.getInstance().getEditSessionFactory().getEditSession(BukkitAdapter.adapt(loc1.getWorld()), -1);
        CuboidRegion cuboidRegion = new CuboidRegion(BlockVector3.at(loc1.getX(), loc1.getY(), loc1.getZ()), BlockVector3.at(loc2.getX(), loc2.getY(), loc2.getZ()));
        editSession.setBlocks((Region)cuboidRegion, BlockTypes.parse(mat.toString()));
        editSession.flushQueue();
    }


    public static Location convertToBukkitLocation(com.plotsquared.core.location.Location location) {
        return new Location(Bukkit.getWorld(location.getWorld()), location.getX(), location.getY(), location.getZ());
    }

    public static String translate(String text) {
        Pattern pattern = Pattern.compile("#[a-fA-F0-9]{6}");
        Matcher matcher = pattern.matcher(text);
        String final_string = text;
        while (matcher.find())
            final_string = final_string.replaceAll(matcher.group(0), ChatColor.of(matcher.group(0)).toString());
        return ChatColor.translateAlternateColorCodes('&', final_string);
    }

    public static boolean isBlockInBlackList(Material mat) {
        if (config.getBoolean("regex")) {
            List<String> blacklist = config.getStringList("blacklist");
            for (int i = 0; i < blacklist.size(); i++) {
                Pattern pattern = Pattern.compile(blacklist.get(i));
                Matcher matcher = pattern.matcher(mat.name());
                if (matcher.find())
                    return true;
            }
            return false;
        }
        return config.getStringList("blacklist").contains(mat.name());
    }
}

