package me.imsanti.dev;

import com.plotsquared.core.location.Location;
import com.plotsquared.core.plot.Plot;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

    public class Floor implements CommandExecutor {
        PlotFloorChanger plugin;

        FileConfiguration config;

        public Floor(PlotFloorChanger plugin, FileConfiguration config) {
            this.plugin = plugin;
            this.config = config;
        }

        public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
            if (args.length == 0) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("Only for players!");
                    return true;
                }
                final Player player = (Player)sender;
                if (!player.getInventory().getItemInMainHand().getType().isBlock()) {
                    player.sendMessage("You have to hold a block in hand!");
                    return true;
                }
                ItemStack item = player.getInventory().getItemInMainHand();
                if (PlotFloorChanger.isBlockInBlackList(item.getType())) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.blacklist")));
                    return true;
                }
                Plot plot = new Location(player.getWorld().getName(), (int) player.getLocation().getX(), (int) player.getLocation().getY(), (int) player.getLocation().getZ()).getPlot();
                if (plot == null) {

                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notonplot")));
                    return true;
                }
                if (!plot.isOwner(player.getUniqueId())) {
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notyourplot")));
                    return true;
                }
                int height = this.config.getInt("floor_height");
                Location corner1 = plot.getCorners()[0];
                Location corner2 = plot.getCorners()[1];
                corner1.setY(height);
                corner2.setY(height);
                PlotFloorChanger.fillBlocks(PlotFloorChanger.convertToBukkitLocation(corner1), PlotFloorChanger.convertToBukkitLocation(corner2), item
                        .getType());
                player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.floor_successful")));
                return true;
            }else {
                final Player player = (Player) sender;
                if(args.length == 1) {
                    final Material material = Material.getMaterial(args[0].toUpperCase());
                    if(material == null) {
                        player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.invalid-material")));
                        return true;
                    }

                    if (PlotFloorChanger.isBlockInBlackList(material)) {
                        player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.blacklist")));
                        return true;
                    }
                    Plot plot = new Location(player.getWorld().getName(), (int) player.getLocation().getX(), (int) player.getLocation().getY(), (int) player.getLocation().getZ()).getPlot();
                    if (plot == null) {

                        player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notonplot")));
                        return true;
                    }
                    if (!plot.isOwner(player.getUniqueId())) {
                        player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.notyourplot")));
                        return true;
                    }
                    int height = this.config.getInt("floor_height");
                    Location corner1 = plot.getCorners()[0];
                    Location corner2 = plot.getCorners()[1];
                    corner1.setY(height);
                    corner2.setY(height);
                    PlotFloorChanger.fillBlocks(PlotFloorChanger.convertToBukkitLocation(corner1), PlotFloorChanger.convertToBukkitLocation(corner2), material
                            );
                    player.sendMessage(PlotFloorChanger.translate(this.config.getString("messages.floor_successful")));
                }
            }
            return true;
        }
    }
